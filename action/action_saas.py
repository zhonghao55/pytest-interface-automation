"""同action_admin"""

import allure
import sys

sys.path.append("..")

from api import  saas_api


class Saas():
    def __init__(self,saas_info):
        self.username = saas_info['user_name']
        self.password = saas_info['password']
        self.saas_api_client = saas_api.Saas()
        result = self.saas_login()
        print("result",result)
        assert result['status_code'] == 200,"总后台管理员登录失败"
        self.token = result['data']['data']['token']
        # self.session = result['data']['token']
    """-------------------------------iShow-admin--------------------------------------------------"""

    @allure.step("总后台账号登录")
    def admin_login(self):
        m = saas_api.Saas()
        return m.saas_login0(self.username,self.password)

    @allure.step("总后台账号登录2")
    def admin_login1(self,**kwargs):
        m = saas_api.Saas()
        return m.saas_login0(**kwargs)

    @allure.step("总后台新增课程")
    def add_kecheng(self,**kwargs):
        return self.saas_api_client.admin_add_kecheng(self.token,**kwargs)
    """-------------------------------iShow-admin--------------------------------------------------"""