import allure
from case.conftest import *
from common.read_yaml import *
from common.logger import Logger
testdata = ReadYaml_anjiekou('dan_data/admin_data/login_data.yml').get_yaml_data()#读取数据
data1 = ReadYaml_anjiekou("duo_data/admin_data/1,login_data.yml").get_yaml_data()#读取数据
data_login_admin = data1['login_admin']

@allure.feature('登录测试用例接口')#测试报告显示测试功能
class Test_login():
    '''测试登录接口'''
    # log = Log()
    @pytest.mark.parametrize("username,password,expect",testdata["test_login_data"],
                            ids = ["正常登录",
                                   "密码为空登录",
                                   "账号为空登录",
                                   "账号错误登录",
                                   "密码错误登录",
                                   "账号存在空格登录",
                                   "密码存在空格登录",
                                   "账号存在特殊符号登录",
                                   "密码存在特殊符号登录",
                                   "账号不完整登录",
                                   "密码不完整登录"
                                    ])#参数化测试用例
    @allure.step('账号，密码登录')#测试报告显示步骤
    @allure.link('http://**********:6009/api/v1/dan_jiekou',name='测试接口')#测试报告显示链接
    def test_login(self,username,password,expect,admin_action):
        ##登录
        login = admin_action.admin_login1(account=username,password=password)
        try:
            pytest.assume(login['status_code'] == 200)
            assert str(expect['code']) in str(login['data']['code']) # 断言验证是否通过
            assert str(expect['message']) in str(login['data']['message']) # 断言验证是否通过
        except AssertionError as e:
            Logger.error('=====>{}用例执行未通过，失败信息{}'.format(data_login_admin['title'], login))
            # 将异常抛出
            raise e
        Logger.info("*************** {}结束执行用例 ***************".format(data_login_admin['title']))


if __name__=="__main__":
    pytest.main(['-s',r'D:\iShow\代码\i-show-jiekou-automation9\case\test_dan_api\test_admin_dan\test_login.py'])
