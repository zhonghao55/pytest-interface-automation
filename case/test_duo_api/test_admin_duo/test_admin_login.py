import sys

import allure
import pytest
import pytest_assume
from common.logger import Logger
sys.path.append("..")
from common.read_yaml import ReadYaml, ReadYaml_anjiekou

data1 = ReadYaml_anjiekou("duo_data/admin_data/1,login_data.yml").get_yaml_data()#读取数据
data_login_admin = data1['login_admin']
@allure.feature('总后台——登录')#测试报告显示测试功能
class Test_DengLu():

    @allure.title("总后台——登录")
    @allure.step('账号，密码正常登录')#测试报告显示步骤
    def test_admin_login(self, admin_action):
        ##z总后台-新增英语课程
        Logger.info("*************** {}开始执行用例 ***************".format(data_login_admin['title']))
        admin_login = admin_action.admin_login1()
        print("admin_login",admin_login)
        try:
            pytest.assume(admin_login['status_code'] == 200)
            assert admin_login['data']['code'] == data_login_admin['except_code']
            assert admin_login['data']['message'] == data_login_admin['except_msg']
        except AssertionError as e:
            Logger.error('=====>{}用例执行未通过，失败信息{}'.format(data_login_admin['title'], admin_login))
            # 将异常抛出
            raise e
        Logger.info("*************** {}结束执行用例 ***************".format(data_login_admin['title']))



if __name__=="__main__":
    pytest.main(['-s',r'D:\iShow\代码\pytest-interface-automation\case\test_duo_api\test_admin_duo\test01_admin_login.py'])